API description:
    - /CreateUser/:
        Create new user and store it to database,
        in case if username provided in request exist - just return user data,
        request method "POST",
        params: UserName - string,
        return: json data contained username form request parameter and
        user id in database, also additional data that provides response status.
    - /PostMessage/:
        Store user message to database. Provides simple validation for request parameters,
        such as - check is parameter are empty or user with provided UserId does not exists.
        request method: "POST",
        params: MessageText - string, UserId - string
        return: json data contained status information and stored message description,
        such as user id, message id, and message itself. In case of error - status indormation
        and error description.
    - /Follow/:
        Append FollowedUser to the list of following user for FollowingUser, in case if
        FollowedUser already in list - do nothing.
        Make some basic validation for parameters, such as, is params are valid user ids
        of is users ids are exists in database.
        request method: "POST",
        params: FollowedUser - string, FollowingUser - string
        return: json data contained status information and message, in case of error -
        simple error description
    - /Unfollow/<FollowingUser>/<UnfollowedUser>/:
        Oppositely to /Follow/. Delete UnfollowedUser user from list of followed users
        for FollowingUser. In case if UnfollowedUser not in list - do nothing. Provides
        some basic validation for parameters, such as, is params are valid user ids
        of is users ids are exists in database. In case of call address like /Unfollow/<FollowingUser>/
        or /Unfollow/<UnfollowedUser>/ or /Unfollow/ - raise 404 error.
        request method: "DELETE"
        params: FollowingUser - string, UnfollowedUser - string, parts of request path
        response: json data contained status information and message, in case of error -
        simple error description
    - /GetFeed/<user_id>/:
        return all messages created by user with id specified as <user_id> path part.
        In case if user with provided user_id not exist return empty set of messages.
        In case if user_id path part not specified - raise 404 error.
        request method: "GET"
        params: user_id as part of path
        response: json data contained status information and message, in case of error -
        simple error description, if success - contained user messages list
    - /GetGlobalFeed/:
        return all posted messages created by all existing users
        request method: "GET"
        params: no params
        return: json data contained status information and messages list

Addition methods, designed just for better visualization:
    - /GetUsers/: return list of all users
    - /GetFollow/<user_id>/: return list of users followed by user_id

Api developed using Flask framework. Decision to use Flask mainly related to
willing to try that framework in some little action, and it's look like a not bad choice for prototyping.
As main database used mongodb - looks like pretty suitable for data structure in this project.

About test. I do not have much experience writing tests so I think my realization not quite right