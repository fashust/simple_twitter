# -*- coding: utf-8 -*- #
"""

"""
import os
import pymongo
from flask.views import MethodView
from flask import Flask, render_template, g, make_response, jsonify
from views import Messages, User, Follow


__author__ = 'fashust'
__email__ = 'fashust.nefor@gmail.com'


DB_HOST = 'localhost'
DB_PORT = 27017
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))


class AppConfig(object):
    """
        flask application config
    """
    DB_HOST = 'localhost'
    DB_PORT = 27017
    DB_NAME = 'simple_twitter'
    DATABASE = pymongo.Connection
    DEBUG = True
    HOST = '0.0.0.0'
    PORT = 8000
    STATIC_FOLDER = os.path.join(PROJECT_ROOT, 'static')
    TEMPLATE_FOLDER = os.path.join(PROJECT_ROOT, 'templates')


class IndexView(MethodView):
    """
        index view
    """
    def get(self):
        """
            get
        """
        return render_template('index.html')


app = Flask(
    __name__,
    static_folder=AppConfig.STATIC_FOLDER,
    template_folder=AppConfig.TEMPLATE_FOLDER
)
app.config.from_object('.'.join([__name__, 'AppConfig']))
app.add_url_rule(
    '/',
    view_func=IndexView.as_view('index')
)
users = User.as_view('Users')
app.add_url_rule(
    '/CreateUser/',
    view_func=users,
    methods=['POST']
)
app.add_url_rule(
    '/GetUsers/',
    view_func=users,
    methods=['GET']
)
app.add_url_rule(
    '/GetFollow/<user_id>/',
    view_func=users,
    methods=['GET']
)
messages = Messages.as_view('Messages')
app.add_url_rule(
    '/PostMessage/',
    view_func=messages,
    methods=['POST']
)
app.add_url_rule(
    '/GetGlobalFeed/',
    view_func=messages,
    methods=['GET']
)
app.add_url_rule(
    '/GetFeed/<user_id>/',
    view_func=messages,
    methods=['GET']
)
follow = Follow.as_view('Follow')
app.add_url_rule(
    '/Follow/',
    view_func=follow,
    methods=['POST']
)
app.add_url_rule(
    '/Unfollow/<FollowingUser>/<UnfollowedUser>/',
    view_func=follow,
    methods=['DELETE']
)


@app.before_request
def before_request():
    """
        before request
    """
    g.db_connection = AppConfig.DATABASE(
        host=AppConfig.DB_HOST,
        port=AppConfig.DB_PORT
    )
    g.db = g.db_connection[AppConfig.DB_NAME]


@app.teardown_request
def teardown_request(exception):
    """
        teardown request
    """
    db_connection = getattr(g, 'db_connection', None)
    if db_connection:
        db_connection.close()


@app.errorhandler(404)
def page_not_found(e):
    """
        page not found handler
    """
    data = {
        'status': False,
        'data': {
            'error': 'Not found',
            'message': 'Requested page not found'
        }
    }
    response = make_response(jsonify(**data))
    response.status_code = 404
    return response


@app.errorhandler(405)
def method_not_allowed(e):
    """
        method not allowed handler
    """
    data = {
        'status': False,
        'data': {
            'error': 'Not allowed',
            'message': 'Requested method not allowed'
        }
    }
    response = make_response(jsonify(**data))
    response.status_code = 405
    return response


if __name__ == '__main__':
    app.run(host=AppConfig.HOST, port=AppConfig.PORT)
