# -*- coding: utf-8 -*- #
"""

"""
import json
from bson import ObjectId
import datetime
import itertools
import pymongo
import unittest
from simple_twitter import app, AppConfig
from unittest import TestCase


__author__ = 'fashust'
__email__ = 'fashust.nefor@gmail.com'


def get_db():
    """
        return db connection
    """
    return pymongo.Connection(
        host=AppConfig.DB_HOST,
        port=AppConfig.DB_PORT
    )[AppConfig.DB_NAME]


def create_test_users(count):
    """
        create test users
    """
    data = [
        {
            'username': ''.join(['user', str(x)]),
            'follow': []
        }
        for x in xrange(count)
    ]
    db = pymongo.Connection(
        host=AppConfig.DB_HOST,
        port=AppConfig.DB_PORT
    )[AppConfig.DB_NAME]
    return list(
        db['users'].find({'_id': {'$in': db['users'].insert(data)}})
    )


def create_test_messages(args):
    """
        create test messages by user
    """
    user, count = args
    data = [{
        'user_id': '{0:s}'.format(user['_id']),
        'message': 'simple message {0:d}'.format(x),
        'datetime': '{0}'.format(datetime.datetime.now())
    } for x in xrange(count)]
    db = pymongo.Connection(
        host=AppConfig.DB_HOST,
        port=AppConfig.DB_PORT
    )[AppConfig.DB_NAME]
    db['messages'].insert(data)
    return data


class SimpleTwitterTestCase(TestCase):
    """
        simple twitter test case
    """
    def setUp(self):
        """
            setup
        """
        AppConfig.DB_NAME = 'test_db'
        self.app = app.test_client()

    def tearDown(self):
        """
            tear down
        """
        pymongo.Connection(
            host=AppConfig.DB_HOST,
            port=AppConfig.DB_PORT
        ).drop_database(AppConfig.DB_NAME)

    def testCreateUser(self):
        """
            test index page response
        """
        response = self.app.post('/CreateUser/')
        response_data = json.loads(response.data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_data['status'], False)
        self.assertIn('error', response_data['data'].keys())
        self.assertIn('message', response_data['data'].keys())

        response = self.app.get('/CreateUser/')
        response_data = json.loads(response.data)
        self.assertEqual(response.status_code, 405)
        self.assertEqual(response_data['status'], False)
        self.assertIn('error', response_data['data'].keys())
        self.assertIn('message', response_data['data'].keys())

        response = self.app.put('/CreateUser/')
        response_data = json.loads(response.data)
        self.assertEqual(response.status_code, 405)
        self.assertEqual(response_data['status'], False)
        self.assertIn('error', response_data['data'].keys())
        self.assertIn('message', response_data['data'].keys())

        response = self.app.delete('/CreateUser/')
        response_data = json.loads(response.data)
        self.assertEqual(response.status_code, 405)
        self.assertEqual(response_data['status'], False)
        self.assertIn('error', response_data['data'].keys())
        self.assertIn('message', response_data['data'].keys())

        response = self.app.post('/CreateUser/', data={'UserName': ''})
        response_data = json.loads(response.data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_data['status'], False)
        self.assertIn('error', response_data['data'].keys())
        self.assertIn('message', response_data['data'].keys())

        response = self.app.post('/CreateUser/', data={'UserName': 'user1'})
        response_data = json.loads(response.data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_data['status'], True)
        self.assertIn('message', response_data['data'].keys())
        self.assertIn('username', response_data['data'].keys())
        self.assertIn('user_id', response_data['data'].keys())
        first_user = response_data['data']

        response = self.app.post(
            '/CreateUser/',
            data={'UserName': first_user['username']}
        )
        response_data = json.loads(response.data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_data['status'], True)
        self.assertIn('message', response_data['data'].keys())
        self.assertIn('username', response_data['data'].keys())
        self.assertIn('user_id', response_data['data'].keys())
        self.assertEqual(
            first_user['user_id'],
            response_data['data']['user_id']
        )
        self.assertEqual(
            first_user['username'],
            response_data['data']['username']
        )

        response = self.app.post('/CreateUser/', data={'UserName': 'user2'})
        response_data = json.loads(response.data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_data['status'], True)
        self.assertIn('message', response_data['data'].keys())
        self.assertIn('username', response_data['data'].keys())
        self.assertIn('user_id', response_data['data'].keys())
        self.assertNotEqual(
            first_user['user_id'],
            response_data['data']['user_id']
        )
        self.assertNotEqual(
            first_user['user_id'],
            response_data['data']['user_id']
        )

    def testPostMessage(self):
        """
            test for post message
        """
        response = self.app.get('/PostMessage/')
        self.assertEqual(response.status_code, 405)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.put('/PostMessage/')
        self.assertEqual(response.status_code, 405)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.delete('/PostMessage/')
        self.assertEqual(response.status_code, 405)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.post('/PostMessage/')
        self.assertEqual(response.status_code, 200)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.post('/PostMessage/', data={'MessageText': ''})
        self.assertEqual(response.status_code, 200)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.post('/PostMessage/', data={'UserId': ''})
        self.assertEqual(response.status_code, 200)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.post(
            '/PostMessage/',
            data={'UserId': '', 'MessageText': ''}
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.post(
            '/PostMessage/',
            data={'UserId': '123', 'MessageText': ''}
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        test_user, = create_test_users(1)
        response = self.app.post(
            '/PostMessage/',
            data={'UserId': str(test_user['_id']), 'MessageText': ''}
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.post(
            '/PostMessage/',
            data={'UserId': str(test_user['_id']), 'MessageText': 'sample'}
        )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(json.loads(response.data)['status'])
        self.assertIn('message', json.loads(response.data)['data'].keys())
        self.assertIn('record', json.loads(response.data)['data'].keys())
        self.assertIn(
            '_id',
            json.loads(response.data)['data']['record'].keys()
        )
        self.assertIn(
            'user_id',
            json.loads(response.data)['data']['record'].keys()
        )
        self.assertIn(
            'message',
            json.loads(response.data)['data']['record'].keys()
        )
        self.assertIn(
            'datetime',
            json.loads(response.data)['data']['record'].keys()
        )
        self.assertEqual(
            str(test_user['_id']),
            json.loads(response.data)['data']['record']['user_id']
        )

    def testFollowUnfollowUser(self):
        """
            test follow unfollow user
        """
        first_user, second_user, third_user = create_test_users(3)
        response = self.app.get('/Follow/')
        self.assertEqual(response.status_code, 405)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.put('/Follow/')
        self.assertEqual(response.status_code, 405)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.delete('/Follow/')
        self.assertEqual(response.status_code, 405)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.post('/Follow/', data={})
        self.assertEqual(response.status_code, 200)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.post('/Follow/', data={'FollowingUser': ''})
        self.assertEqual(response.status_code, 200)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.post('/Follow/', data={'FollowedUser': ''})
        self.assertEqual(response.status_code, 200)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.post(
            '/Follow/',
            data={'FollowedUser': '', 'FollowingUser': ''}
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.post(
            '/Follow/',
            data={'FollowedUser': '123', 'FollowingUser': ''}
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.post(
            '/Follow/',
            data={'FollowedUser': '', 'FollowingUser': '123'}
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.post(
            '/Follow/',
            data={
                'FollowedUser': str(first_user['_id']),
                'FollowingUser': '123'
            }
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.post(
            '/Follow/',
            data={
                'FollowedUser': '123',
                'FollowingUser': str(first_user['_id'])
            }
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.post(
            '/Follow/',
            data={
                'FollowedUser': '1' * 24,
                'FollowingUser': str(first_user['_id'])
            }
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.post(
            '/Follow/',
            data={
                'FollowedUser': str(second_user['_id']),
                'FollowingUser': str(first_user['_id'])
            }
        )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(json.loads(response.data)['status'])
        self.assertIn('message', json.loads(response.data)['data'].keys())
        self.assertEqual('OK', json.loads(response.data)['data']['message'])
        first_user = get_db()['users'].find_one(
            {'_id': first_user['_id']}
        )
        self.assertIsInstance(first_user['follow'], list)
        self.assertIn(ObjectId(second_user['_id']), first_user['follow'])

        response = self.app.post(
            '/Follow/',
            data={
                'FollowedUser': str(third_user['_id']),
                'FollowingUser': str(first_user['_id'])
            }
        )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(json.loads(response.data)['status'])
        self.assertIn('message', json.loads(response.data)['data'].keys())
        self.assertEqual('OK', json.loads(response.data)['data']['message'])
        first_user = get_db()['users'].find_one(
            {'_id': first_user['_id']}
        )
        self.assertIsInstance(first_user['follow'], list)
        self.assertIn(ObjectId(second_user['_id']), first_user['follow'])
        self.assertIn(ObjectId(third_user['_id']), first_user['follow'])

        response = self.app.delete('/Unfollow/')
        self.assertEqual(response.status_code, 404)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.delete('/Unfollow/sample_path/')
        self.assertEqual(response.status_code, 404)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.get('/Unfollow/sample_path/sample_path/')
        self.assertEqual(response.status_code, 405)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.post('/Unfollow/sample_path/sample_path/')
        self.assertEqual(response.status_code, 405)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.put('/Unfollow/sample_path/sample_path/')
        self.assertEqual(response.status_code, 405)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.delete(
            '/Unfollow/{0}/{0}/'.format('sample_path'),
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.delete(
            '/Unfollow/{0:s}/{1:s}/'.format(first_user['_id'], 'sample_path')
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.delete(
            '/Unfollow/{1:s}/{0:s}/'.format(first_user['_id'], 'sample_path')
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.delete(
            '/Unfollow/{1:s}/{0:s}/'.format(first_user['_id'], '1' * 24)
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        for x in xrange(2):
            response = self.app.delete(
                '/Unfollow/{0:s}/{1:s}/'.format(
                    first_user['_id'],
                    third_user['_id']),
            )
            self.assertEqual(response.status_code, 200)
            self.assertTrue(json.loads(response.data)['status'])
            self.assertIn('message', json.loads(response.data)['data'].keys())
            self.assertEqual(
                json.loads(response.data)['data']['message'],
                'OK'
            )
            first_user = get_db()['users'].find_one(
                {'_id': first_user['_id']}
            )
            self.assertIsInstance(first_user['follow'], list)
            self.assertIn(ObjectId(second_user['_id']), first_user['follow'])
            self.assertNotIn(ObjectId(third_user['_id']), first_user['follow'])

        response = self.app.delete(
            '/Unfollow/{0:s}/{1:s}/'.format(
                first_user['_id'],
                second_user['_id']),
        )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(json.loads(response.data)['status'])
        self.assertIn('message', json.loads(response.data)['data'].keys())
        self.assertEqual(
            json.loads(response.data)['data']['message'],
            'OK'
        )
        first_user = get_db()['users'].find_one(
            {'_id': first_user['_id']}
        )
        self.assertIsInstance(first_user['follow'], list)
        self.assertNotIn(ObjectId(second_user['_id']), first_user['follow'])
        self.assertNotIn(ObjectId(third_user['_id']), first_user['follow'])
        self.assertListEqual([], first_user['follow'])

    def testGetMessages(self):
        """
            get messages
        """
        users = create_test_users(3)
        messages = map(create_test_messages, zip(users, [3] * 3))

        response = self.app.post('/GetGlobalFeed/')
        self.assertEqual(response.status_code, 405)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.put('/GetGlobalFeed/')
        self.assertEqual(response.status_code, 405)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.delete('/GetGlobalFeed/')
        self.assertEqual(response.status_code, 405)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.get('/GetGlobalFeed/')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(json.loads(response.data)['status'])
        self.assertIn('messages', json.loads(response.data)['data'].keys())
        self.assertIsInstance(
            json.loads(response.data)['data']['messages'],
            list
        )
        response_messages = json.loads(response.data)['data']['messages']
        messages_dif = set(
            [x['_id'] for x in sorted(response_messages)]
        ) - set(
            [u'{0:s}'.format(x['_id']) for x in sorted(itertools.chain(*messages))]
        )
        self.assertEqual(set([]), messages_dif)
        self.assertEqual(len(response_messages), 9)
        [
            self.assertEqual(
                sorted([u'{0}'.format(x['_id']) for x in l]),
                sorted([
                    x['_id']
                    for x in response_messages
                    if x['user_id'] == u'{0:s}'.format(l[0]['user_id'])
                ])
            )
            for l in messages
        ]

        response = self.app.post('/GetFeed/')
        self.assertEqual(response.status_code, 404)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.put('/GetFeed/')
        self.assertEqual(response.status_code, 404)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.delete('/GetFeed/')
        self.assertEqual(response.status_code, 404)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.get('/GetFeed/')
        self.assertEqual(response.status_code, 404)
        self.assertFalse(json.loads(response.data)['status'])
        self.assertIn('error', json.loads(response.data)['data'].keys())
        self.assertIn('message', json.loads(response.data)['data'].keys())

        response = self.app.get('/GetFeed/sample/')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(json.loads(response.data)['status'])
        self.assertIn('messages', json.loads(response.data)['data'].keys())
        self.assertIsInstance(
            json.loads(response.data)['data']['messages'],
            list
        )
        self.assertEqual(json.loads(response.data)['data']['messages'], [])

        response = self.app.get('/GetFeed/{0}/'.format('1' * 24))
        self.assertEqual(response.status_code, 200)
        self.assertTrue(json.loads(response.data)['status'])
        self.assertIn('messages', json.loads(response.data)['data'].keys())
        self.assertIsInstance(
            json.loads(response.data)['data']['messages'],
            list
        )
        self.assertEqual(json.loads(response.data)['data']['messages'], [])
        [
            (
                self.assertEqual(
                    self.app.get(
                        '/GetFeed/{0}/'.format(data[0]['_id'])
                    ).status_code,
                    200
                ),
                self.assertTrue(json.loads(
                    self.app.get('/GetFeed/{0}/'.format(data[0]['_id'])).data
                )['status']),
                self.assertEqual(
                    sorted(
                        x['_id']
                        for x in response_messages
                        if x['user_id'] == u'{}'.format(data[0]['_id'])
                    ),
                    sorted([u'{0}'.format(x['_id']) for x in data[1]])
                )
            )
            for data in zip(users, messages)
        ]


if __name__ == '__main__':
    unittest.main()