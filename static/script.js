/**
 * Created by fashust on 09.11.13.
 */
$(document).ready(function() {
    var createUserForm = $('.createUser'),
        postMessageForm = $('.postMessage'),
        messageHolder = $('.messageHolder'),
        user_id = null,
        username = null;

    get_messages(null);

    $(document).on('click', '.createUser input[type=button]', function() {
        $.ajax({
            type: 'POST',
            url: '/CreateUser/',
            data: createUserForm.serialize(),
            success: function(response) {
                var status = response.status,
                    data = response.data;
                if (status) {
                    user_id = data.user_id;
                    username = data.username;
                    postMessageForm.find('input[name=UserId]').val(user_id);
                    createUserForm.hide()
                    messageHolder.hide();
                    get_messages(user_id);
                    postMessageForm.show();
                    get_follow(user_id);
                } else {
                    alert(data.error + '\n' + data.message);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            },
            dataType: 'json'
        });
        return false;
    });

    $(document).on('click', '.postMessage input[type=button]', function() {
        $.ajax({
            type: 'POST',
            url: '/PostMessage/',
            data: postMessageForm.serialize(),
            success: function(response) {
                var status = response.status,
                    data = response.data;
                if (status) {
                    get_messages(user_id);
                } else {
                    alert(data.error + '\n' + data.message);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            },
            dataType: 'json'
        });
        return false;
    });

    function get_messages(user_id) {
        $.ajax({
        type: 'GET',
        url: user_id == null ? '/GetGlobalFeed/' : '/GetFeed/' + user_id + '/',
        dataType: 'json',
        success: function(response) {
            var status = response.status,
                data = response.data,
                html = '';
                if (status) {
                    var messages = data.messages;
                    for(var i = 0; i <= messages.length - 1; i++) {
                        var record = messages[i];
                        html += '<div data-id="' + record._id + '" data-user="' + record.user_id + '" class="message">';
                        html += '<span class="date">' + record.datetime + '</span>';
                        html += '<span class="txt">' + record.message + '</span>';
                    }
                    messageHolder.html(html);
                    messageHolder.show();
                } else {
                    alert(data.error + '\n' + data.message);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            }
        });
    }

    function get_follow(user_id) {
        $.ajax({
            type: 'GET',
            url: '/GetFollow/' + user_id + '/',
            dataType: 'json',
            success: function(response) {
                console.log(response);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            }
        })
    }
});