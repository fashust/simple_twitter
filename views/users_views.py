# -*- coding: utf-8 -*- #
"""
    create user handler
"""
import re
from bson import ObjectId
from flask import request, jsonify, g
from flask.views import MethodView
from views import OBJECTID_RE, objid_to_str


__author__ = 'fashust'
__email__ = 'fashust.nefor@gmail.com'


class User(MethodView):
    """
        create user view
    """
    def __init__(self):
        """
            init
        """
        self.response = {
            'status': True,
            'data': {}
        }

    def post(self):
        """
            post
        """
        username = request.form.get('UserName', None)
        if not username:
            self.response['status'] = False
            self.response['data'].update({
                'error': 'invalid username',
                'message': 'username empty or not valid format'
            })
        else:
            users_collection = g.db['users']
            user = users_collection.find_one({'username': username})
            if not user:
                user = {'username': username, 'follow': []}
                users_collection.insert(user)
            self.response['data'].update({
                'message': 'OK',
                'user_id': str(user['_id']),
                'username': username
            })
        return jsonify(**self.response)

    def get(self, user_id=None):
        """
            get
        """
        users_collection = g.db['users']
        if not user_id:
            self.response['data'].update({
                'users': [
                    {'_id': str(x['_id']), 'username': x['username']}
                    for x in users_collection.find(
                        {},
                        {'_id': 1, 'username': 1}
                    )
                ]
            })
        else:
            if not re.match(OBJECTID_RE, user_id):
                self.response['status'] = False
                self.response['data'].apdate({
                    'error': 'invalid user_id',
                    'massage': 'notvalid user_id in request'
                })
            else:
                follow = map(
                    objid_to_str,
                    users_collection.find(
                        {
                            '_id': {
                                '$in': users_collection.find_one(
                                    {'_id': ObjectId(user_id)},
                                    {'follow': 1, '_id': 0}
                                )
                            }
                        },
                        {'_id': 1, 'username': 1}
                    )
                )
                self.response['data'].update({
                    'message': 'OK',
                    'follow': follow
                })
        return jsonify(**self.response)