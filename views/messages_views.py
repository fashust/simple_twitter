# -*- coding: utf-8 -*- #
"""
    post message handler
"""
import re
import datetime
from flask import g, jsonify, request
from flask.views import MethodView
from views import OBJECTID_RE, objid_to_str
from bson.objectid import ObjectId


__author__ = 'fashust'
__email__ = 'fashust.nefor@gmail.com'


class Messages(MethodView):
    """
        post message
    """
    def __init__(self):
        """
            init
        """
        self.response = {
            'status': True,
            'data': {}
        }

    def post(self):
        """
            post
        """
        message = request.form.get('MessageText', None)
        user_id = request.form.get('UserId', '')
        users_collection = g.db['users']
        if not re.match(OBJECTID_RE, user_id):
            user = None
        else:
            user_id = ObjectId(user_id)
            user = users_collection.find_one({'_id': user_id})
        if not user:
            self.response['status'] = False
            self.response['data'].update({
                'error': 'Not found',
                'message': 'User not found or not valid user_id'
            })
        elif not message:
            self.response['status'] = False
            self.response['data'].update({
                'error': 'Not valid message',
                'message': 'Not valid message format or message empty'
            })
        else:
            messages_collection = g.db['messages']
            rec = {
                'user_id': user_id,
                'message': message,
                'datetime': str(datetime.datetime.now())
            }
            messages_collection.insert(rec)
            rec['_id'] = str(rec['_id'])
            rec['user_id'] = str(rec['user_id'])
            self.response['data'].update({
                'message': 'OK',
                'record': rec
            })
        return jsonify(**self.response)

    def get(self, user_id=None):
        """
            get
        """
        messages = []
        messages_collection = g.db['messages']
        if not user_id:
            messages = map(
                objid_to_str,
                messages_collection.find({}).sort('datetime', -1)
            )
        elif re.match(OBJECTID_RE, user_id):
            messages = map(
                objid_to_str,
                messages_collection.find(
                    {'user_id': ObjectId(user_id)}
                ).sort('datetime', -1)
            )
        self.response['data'].update({
            'messages': messages
        })
        return jsonify(**self.response)