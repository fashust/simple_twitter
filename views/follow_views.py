# -*- coding: utf-8 -*- #
"""
    follow user handler
"""
import re
from flask import request, g, jsonify
from flask.views import MethodView
from views import OBJECTID_RE
from bson.objectid import ObjectId


__author__ = 'fashust'
__email__ = 'fashust.nefor@gmail.com'


class Follow(MethodView):
    """
        follow\unfollow user
    """
    def __init__(self):
        """
            init
        """
        self.response = {
            'status': True,
            'data': {}
        }

    def post(self):
        """
            post
            FollowingUser, FollowedUser
        """
        followingUser = request.form.get('FollowingUser', '')
        followedUser = request.form.get('FollowedUser', '')
        if not re.match(OBJECTID_RE, followedUser) or \
                not re.match(OBJECTID_RE, followingUser):
            self.response['status'] = False
            self.response['data'].update({
                'error': 'Invalid reqest params',
                'message': 'Request params a empty or not valid format'
            })
        else:
            followedUser = ObjectId(followedUser)
            followingUser = ObjectId(followingUser)
            users_collection = g.db['users']
            users = list(users_collection.find({
                '_id': {'$in': [followedUser, followingUser]}
            }).sort('_id', -1 if followedUser > followingUser else 1))
            if len(users) != 2:
                self.response['status'] = False
                self.response['data'].update({
                    'error': 'User not found',
                    'message': 'One of users not found'
                })
            else:
                followedUser, followingUser = users
                if followedUser['_id'] not in followingUser['follow']:
                    followingUser['follow'].append(followedUser['_id'])
                    users_collection.save(followingUser)
                self.response['data'].update({
                    'message': 'OK',
                })
        return jsonify(**self.response)

    def delete(self, FollowingUser='', UnfollowedUser=''):
        """
            delete
        """
        followingUser = FollowingUser
        unfollowedUser = UnfollowedUser
        if not re.match(OBJECTID_RE, unfollowedUser) or \
                not re.match(OBJECTID_RE, followingUser):
            self.response['status'] = False
            self.response['data'].update({
                'error': 'Invalid reqest params',
                'message': 'Request params a empty or not valid format'
            })
        else:
            users_collection = g.db['users']
            followingUser = users_collection.find_one(
                {'_id': ObjectId(followingUser)}
            )
            if not followingUser:
                self.response['status'] = False
                self.response['data'].update({
                    'error': 'User not found',
                    'message': 'Following user not found'
                })
            else:
                followingUser['follow'] = [
                    x for x in followingUser['follow'] if x != ObjectId(
                        unfollowedUser
                    )
                ]
                users_collection.save(followingUser)
                self.response['data'].update({
                    'message': 'OK'
                })
        return jsonify(**self.response)