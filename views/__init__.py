# -*- coding: utf-8 -*- #
"""
    flask views
"""

__author__ = 'fashust'
__email__ = 'fashust.nefor@gmail.com'


OBJECTID_RE = r'^[0-9,a-z]{24}'


def objid_to_str(item):
    """
        ObjectId to str
    """
    item['_id'] = str(item['_id'])
    if item.get('user_id', None):
        item['user_id'] = str(item['user_id'])
    return item


from .users_views import User
from .messages_views import Messages
from .follow_views import Follow